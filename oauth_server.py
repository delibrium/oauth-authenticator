import falcon
import requests
from dotenv import load_dotenv
import psycopg2
import psycopg2.extras
from psycopg2.extras import Json
import logging
import datetime
import json
import os

load_dotenv()

GITHUB_CLIENT_ID = os.getenv('GITHUB_CLIENT_ID')
GITHUB_CLIENT_SECRET = os.getenv('GITHUB_CLIENT_SECRET')
GOOGLE_CLIENT_ID = os.getenv('GOOGLE_CLIENT_ID')
GOOGLE_CLIENT_SECRET = os.getenv('GOOGLE_CLIENT_SECRET')
FACEBOOK_CLIENT_ID = os.getenv('FACEBOOK_CLIENT_ID')
FACEBOOK_CLIENT_SECRET = os.getenv('FACEBOOK_CLIENT_SECRET')
MAILGUN_API_KEY = os.getenv('MAILGUN_API_KEY')
MAILGUN_API_DOMAIN = os.getenv('MAILGUN_API_DOMAIN')
MAILGUN_DOMAIN = os.getenv('MAILGUN_DOMAIN')
MAIL_NOTIFY = os.getenv('MAIL_NOTIFY')
LOGFILE = os.getenv('LOGFILE')

logging.basicConfig(filename=LOGFILE, format='%(asctime)s %(levelname)s => %(message)s', level=logging.DEBUG)

db_conn = psycopg2.connect(
    dbname=os.getenv("AULA_DB_NAME"),
    user=os.getenv("AULA_DB_USER"),
    password=os.getenv("AULA_DB_PASS"),
    host=os.getenv("AULA_DB_HOST"),
    port=int(os.getenv("AULA_DB_PORT"))
)

# Open a cursor to perform database operations
db_cur = db_conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)


class OauthProvider(object):

    __AUTHORIZER_NAME__ = 'AUTHORIZER_NAME'

    def __init__(self, db_con, db_cur):
        self.db_conn = db_con
        self.db_cur = db_cur

    def get_oauth_user(self, code):
        pass

    def create_user(self,
                    user,
                    school_id,
                    resp):

        first_name = user.get('first_name', '')
        last_name = user.get('last_name', '')
        username = user.get('username', '')
        authorizer_provider = user['authorizer_provider']
        authorizer_id = user['authorizer_id']
        email = user.get('email', '')

        sql_create_user = '''select aula_secure.add_oauth_user(
                        %s, %s, %s, %s, %s, %s, 'school_guest', %s)'''
        try:
            self.db_cur.execute(sql_create_user, [
                first_name,
                last_name,
                username,
                authorizer_provider,
                authorizer_id,
                email,
                school_id
            ])
            res = self.db_cur.fetchall()
            self.send_new_user_notification(
                username, first_name, last_name, authorizer_provider, school_id)
            self.db_conn.commit()

            user_id = res[0]['add_oauth_user']['user_id']['id']
            return user_id
        except:
            logging.error("Error creating user: first_name: {}, last_name: {}, username: {}, authorizer_provider: {}, authorizer_id: {}, email: {}, school_id: {}".format(
                first_name, last_name, username, authorizer_provider, authorizer_id, email, school_id))
            self.db_conn.rollback()

    def get_user_in_db(self, user, school_id):

        sql_check_user = '''select id from aula.users where school_id = %s
                                and authorizer->%s = %s;'''
        try:
            self.db_cur.execute(sql_check_user, [
                school_id, self.__AUTHORIZER_NAME__, '"{}"'.format(user['authorizer_id'])])
            user_db = self.db_cur.fetchall()
        except:
            logging.error(
                "Error getting user {} in the school {}".format(user, school_id))
            self.db_conn.rollback()
            return None

        if len(user_db) == 1:
            return user_db[0]['id']
        else:
            return None

    def get_token(self, school_id, user_id, resp):
        sql_get_token = '''select aula_secure.oauth_login(%s, %s);'''
        try:
            self.db_cur.execute(sql_get_token, [school_id, user_id])
            res = self.db_cur.fetchall()

            resp.set_header(
                'Access-Control-Expose-Headers', 'Authorization')
            resp.set_header('Authorization', 'Bearer {}'.format(
                res[0]['oauth_login']['token']))
            resp.body = json.dumps(res[0]['oauth_login']['data'])
            return resp
        except:
            logging.error("Error generating token: school_id: {}, user_id: {}".format(
                school_id, user_id))
            self.db_conn.rollback()
            return resp

    def send_new_user_notification(self, username, first_name, last_name, authorizer_provider, school_id):
        # Get school
        sql_get_school = 'select name from aula.school where id = %s'

        try:
            self.db_cur.execute(sql_get_school, [school_id])
            res = self.db_cur.fetchall()
        except:
            logging.error('Error getting user school for notification')
            self.db_conn.rollback()

        if len(res) > 0:
            school_name = res[0]['name']
        else:
            school_name = ''

        date = datetime.datetime.now()
        time = '{}:{}'.format(date.hour, date.minute)
        message_title = "New user on school {} registered".format(school_name)
        message = '''
            A new user was registered in the school {} on {}/{}/{} at {}.

            User information:

            Username: {}
            First name: {}
            Last name: {}
            Social Identity provider: {}

            Please login as administrator and assign the user to some group or class.
        '''.format(
            school_name,
            date.day,
            date.month,
            date.year,
            time,
            username,
            first_name,
            last_name,
            authorizer_provider)

        try:
            res = requests.post(
                "{}/messages".format(MAILGUN_API_DOMAIN),
                auth=("api", MAILGUN_API_KEY),
                data={"from": "New User Registered <new-user-register@{}>".format(MAILGUN_DOMAIN),
                      "to": [MAIL_NOTIFY, 'new-user-registered@mg.aula.de', 'e.zanella.alvarenga@gmail.com'],
                      "subject": message_title,
                      "text": message})
        except:
            loggin.error("Error sending email: to: {}, subject: {}, text: {}".format(
                MAIL_NOTIFY, message_title, message))


class GithubOauth(OauthProvider):
    __AUTHORIZER_NAME__ = 'github'

    def get_oauth_user(self, code):
        headers = {
            'Accept': 'application/json'
        }
        r = requests.post('https://github.com/login/oauth/access_token',
                          headers=headers,
                          data={
                              'client_id': GITHUB_CLIENT_ID,
                              'client_secret': GITHUB_CLIENT_SECRET,
                              'code': code
                          })
        r_json = r.json()

        # Get User
        headers = {
            'Accept': 'application/json',
            'Authorization': 'token {}'.format(r_json['access_token'])
        }
        r = requests.get('https://api.github.com/user', headers=headers)
        user_info = r.json()

        first_name, last_name = '', ''
        if user_info['name'] is not None:
            if ' ' in user_info['name']:
                first_name, last_name = user_info['name'].split(' ', 1)
            else:
                first_name = user_info['name']
                last_name = ''

        user = {
            'first_name': first_name,
            'last_name': last_name,
            'username': user_info['login'],
            'authorizer_provider': self.__AUTHORIZER_NAME__,
            'authorizer_id': user_info['login'],
            'email':  user_info.get('email', '')
        }

        return user


class GoogleOauth(OauthProvider):
    __AUTHORIZER_NAME__ = 'google'

    def get_oauth_user(self, code):
        headers = {
            'Accept': 'application/json'
        }
        r = requests.post('https://www.googleapis.com/oauth2/v4/token',
                          headers=headers,
                          data={
                              'grant_type': 'authorization_code',
                              'client_id': GOOGLE_CLIENT_ID,
                              'client_secret': GOOGLE_CLIENT_SECRET,
                              'code': code,
                              'redirect_uri': 'https://social.aula.de/oauth/google/',
                          })
        r_json = r.json()

        # Get User
        r = requests.get(
            'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + r_json['id_token'])
        user_info = r.json()

        if ' ' in user_info['name']:
            first_name, last_name = user_info['name'].split(' ', 1)
        else:
            first_name = user_info.get('name', '')
            last_name = ''

        user = {
            'first_name': first_name,
            'last_name': last_name,
            'username': user_info['email'],
            'authorizer_provider': self.__AUTHORIZER_NAME__,
            'authorizer_id': user_info['email'],
            'email': user_info['email']
        }

        return user


class FacebookOauth(OauthProvider):
    __AUTHORIZER_NAME__ = 'facebook'

    def get_oauth_user(self, code):
        r = requests.get('https://graph.facebook.com/v8.0/oauth/access_token?\
            client_id=' + FACEBOOK_CLIENT_ID + '&\
            client_secret=' + FACEBOOK_CLIENT_SECRET + '&\
            code=' + code + '&\
            redirect_uri=https://social.aula.de/oauth/facebook/')
        r_json = r.json()

        # Get User
        r = requests.get('https://graph.facebook.com/me?\
            access_token=' + r_json['access_token'])
        user_id = r.json()

        r = requests.get('https://graph.facebook.com/' + user_id['id'] + '?\
            fields=name,email&\
            access_token=' + r_json['access_token'])
        user_info = r.json()

        if ' ' in user_info['name']:
            first_name, last_name = user_info['name'].split(' ', 1)
        else:
            first_name = user_info.get('name', '')
            last_name = ''

        user = {
            'first_name': first_name,
            'last_name': last_name,
            'username': first_name[0: 3] + last_name[0: 3],
            'authorizer_provider': self.__AUTHORIZER_NAME__,
            'authorizer_id': user_info['id'],
            'email':  user_info.get('email', '')
        }

        return user


github_oauth = GithubOauth(db_conn, db_cur)
google_oauth = GoogleOauth(db_conn, db_cur)
facebook_oauth = FacebookOauth(db_conn, db_cur)


class OauthGetToken(object):

    __authorizers__ = {
        'github': github_oauth,
        'google': google_oauth,
        'facebook': facebook_oauth
    }

    def on_post(self, req, resp):
        """Ask token for Identity server"""
        code = req.media.get('code')
        authorizer = req.media.get('authorizer')
        school_id = req.media.get('school_id')

        if 'error' in req.media:
            resp.body = json.dumps({'error': 'Not authorized'})
        else:
            if authorizer not in self.__authorizers__:
                raise Exception(
                    'Authorizer "{}" not implemented'.format(authorizer))

            # Get user info from OAuth provider
            user = self.__authorizers__[authorizer].get_oauth_user(code)

            # Check if user exists in DB
            user_id = self.__authorizers__[
                authorizer].get_user_in_db(user, school_id)
            # If not create it and get new user id
            if user_id is None:
                user_id = self.__authorizers__[
                    authorizer].create_user(user, school_id, resp)

            resp = self.__authorizers__[authorizer].get_token(
                school_id, user_id, resp)


app = falcon.API()

oauth = OauthGetToken()
app.add_route('/', oauth)
